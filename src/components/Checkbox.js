import React, { Component } from 'react';

class Checkbox extends Component {

    state = {
        isChecked: true
    }

    // Generate checkbox markup with default check, name, text etc.
    getCheckboxMarkup = (data) => {
        return (
            <form>
                {
                    data.fields.map((checkbox, idx) => {
                        return (<label key={checkbox.id}>
                            <input type="checkbox" name={checkbox.name} value={checkbox.value} defaultChecked={checkbox.checked} onChange={this.toggleChange} />
                            {checkbox.text}
                        </label>)
                    })
                }
            </form>
        )
    }

    // As at least one checkbox is selected by default for good UX purpose, no need to disable next button
    componentDidMount() {
        const { isNextDisabled } = this.props;
        isNextDisabled(false);
    }

    // Handle change for checkbox
    toggleChange = () => {
        this.setState({
          isChecked: !this.state.isChecked,
        });
      }

    render() {
        const { data } = this.props;
        return this.getCheckboxMarkup(data);
    }
}

export default Checkbox;