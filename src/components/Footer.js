import React, { Component } from 'react';

class Footer extends Component {
    // Show Submit button instead of disabling next for good UX purpose
    getButton = () => {
        const { onNext, onSubmit, isNextDisabled, currentSlide, totalSlides } = this.props;
        if(currentSlide === totalSlides) {
            return <button onClick={onSubmit}>Submit</button>
        } else {
            return <button onClick={onNext} disabled={isNextDisabled}>Next →</button>
        }
    }
    render() {
        const { onPrev, isPrevDisabled } = this.props;
        return (
            <div className="form-controls">
                <button onClick={onPrev} disabled={isPrevDisabled}>← Previous</button>
                {this.getButton()}
            </div>
        );
    }
}

export default Footer;