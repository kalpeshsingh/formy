import React, { Component } from 'react';
import TextArea from './TextArea';
import Radio from './Radio';
import Checkbox from './Checkbox';

class Body extends Component {

    // Render specific component based on question type
    getSpecificComponent = (data) => {
        const { question_type:type } = data
        if (type === 'TextQuestion') {
            return <TextArea key={data.id} data={data} isNextDisabled={this.isNextDisabled}/>;
        } else if (type === 'RadioQuestion') {
            return <Radio key={data.id} data={data} isNextDisabled={this.isNextDisabled}/>
        } else if(type === 'CheckboxQuestion') {
            return <Checkbox key={data.id} data={data} isNextDisabled={this.isNextDisabled}/>
        }
    }

    // Get callback from component and update app state
    isNextDisabled = (isDisabled) => {
        const { setButtonsState } = this.props;
        setButtonsState(isDisabled);
    }

    render() {
        const { data } = this.props;
        return (
            <div className="form-body">
                {this.getSpecificComponent(data)}
            </div>
        );
    }
}

export default Body;