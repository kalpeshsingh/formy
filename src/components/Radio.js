import React, { Component } from 'react';

class Radio extends Component {

    // Set initial selection
    state = {
        currentSelection: 0
    }

    // Generate radio markup with default check, name, text etc.
    getRadioMarkup = (data) => {
        return (
            <form>
                {
                    data.fields.map((radio, idx) => {
                        return (<label key={radio.id}>
                            <input type="radio" name={radio.name} value={radio.value} checked={this.state.currentSelection === radio.value} onChange={this.handleChange} />
                            {radio.text}
                        </label>)
                    })
                }
            </form>
        )
    }

    // As at least one radio is selected by default for good UX purpose, no need to disable next button
    componentDidMount() {
        const { isNextDisabled } = this.props;
        isNextDisabled(false);
    }

    // Handle change for radio
    handleChange = (e) => {
        this.setState({
            currentSelection: +e.currentTarget.value
        });
    }

    render() {
        const { data } = this.props;
        return this.getRadioMarkup(data);
    }
}

export default Radio;