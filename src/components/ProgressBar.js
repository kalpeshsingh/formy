import React, { Component } from 'react';

class ProgressBar extends Component {
    render() {
        const { currentSlide, totalSlides } = this.props;
        const value = `${currentSlide * 100 / totalSlides}%`;
        const style = {
            "width": value
        }
        return (
            <div className="progress">
                <div className="progress-bar" role="progressbar" aria-valuenow={this.props.currentSlide} aria-valuemin="0" aria-valuemax="100" style={ style }></div>
            </div>
        );
    }
}

export default ProgressBar;