import React, { Component } from 'react';

class TextArea extends Component {

    // Set initial value and update when user input
    state = {
        value: ''
    }

    // Handles user input
    handleKeyup = (e) => {
        this.setState({
            value: e.currentTarget.value
        });
    }

    charsToGo = () => {
        const { min_char_length:minLength, is_required } = this.props.data;
        const currentLength = this.state.value.length;
        if( is_required && currentLength < minLength ) {
            return <div className="form-hint">{minLength - currentLength} chars more to go...</div>
        } else {
            return null;
        }
    }

    // Handles next button state
    componentDidMount() {
        const { data, isNextDisabled } = this.props;

        let valueLength = this.state.value.length;
        if(!data.is_required || data.min_char_length <= valueLength) {
            isNextDisabled(false);
        } else {
            isNextDisabled(true);
        }
    }

    // Handles next button state with validation as constraint
    componentDidUpdate() {
        const { data, isNextDisabled } = this.props;
        let valueLength = this.state.value.length;
        if(!data.is_required || data.min_char_length <= valueLength) {
            isNextDisabled(false);
        } else {
            isNextDisabled(true);
        }
    }

    render() {
        return (
            <div>
                <textarea value={this.state.value} onChange={this.handleKeyup} autoFocus  />
                {this.charsToGo()}
            </div>
        );
    }
}

export default TextArea;