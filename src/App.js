import React, { Component } from 'react';
import './App.css';
import { data } from './data';
import ProgressBar from './components/ProgressBar';
import Header from './components/Header';
import Body from './components/Body';
import Footer from './components/Footer';

class App extends Component {

  /** App state to manage - 
    1. Prev/next buttons state
    2. Progress bar
    3. Passing data to body
  **/
  state = {
    totalSlides: data.questions.length,
    currentSlide: 1,
    isPrevDisabled: true,
    isNextDisabled: true,
    data
  }

  // To handle next button state, managing next slide
  onNext = () => {
    let currentSlide = this.state.currentSlide;
    let totalSlides = this.state.totalSlides;

    if (currentSlide < totalSlides) {
      currentSlide++;
      this.setState({
        currentSlide,
        isPrevDisabled: false
      });
    } else {
      this.setState({
        isNextDisabled: true
      });
    }
    if (currentSlide === totalSlides) {
      this.setState({
        isNextDisabled: true
      });
    }
  }

  // To handle prev button state, managing prev slide
  onPrev = () => {
    let currentSlide = this.state.currentSlide;

    if (currentSlide !== 1) {
      currentSlide--;
      this.setState({
        currentSlide,
        isNextDisabled: false
      });
    } else {
      this.setState({
        isPrevDisabled: true
      });
    }
    if (currentSlide === 1) {
      this.setState({
        isPrevDisabled: true
      });
    }
  }

  // To update disable state of next button
  setButtonsState = (isNextDisabled) => {
    const { currentSlide, totalSlides } = this.state;
    if (this.state.isNextDisabled !== isNextDisabled && currentSlide !== totalSlides) {
      this.setState({
        isNextDisabled
      });
    }
  }

  render() {
    const { currentSlide, totalSlides, isPrevDisabled, isNextDisabled, data } = this.state;
    return (
      <div className="container">
        <div className="form-container">
          <ProgressBar currentSlide={currentSlide} totalSlides={totalSlides} />
          <Header title={data.title} />
          <div className="form-subtitle">Q. {data["questions"][currentSlide - 1].prompt}</div>
          <Body data={data["questions"][currentSlide - 1]} setButtonsState={this.setButtonsState} />
          <Footer onNext={this.onNext} onPrev={this.onPrev} isPrevDisabled={isPrevDisabled} isNextDisabled={isNextDisabled} currentSlide={currentSlide} totalSlides={totalSlides} />
        </div>
      </div>
    );
  }
}

export default App;
