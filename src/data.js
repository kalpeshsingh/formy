export const data = {
    "title": "This is a title for the form Header",
    "questions": [
      {
        "id": 2447,
        "question_type": "TextQuestion",
        "prompt": "What is your first answer?",
        "is_required": false,
        "min_char_length": 15
      },
      {
        "id": 2448,
        "question_type": "TextQuestion",
        "prompt": "What is your second answer?",
        "is_required": true,
        "min_char_length": 100
      },
      {
        "id": 2500,
        "question_type": "TextQuestion",
        "prompt": "What is your third answer?",
        "is_required": true,
        "min_char_length": 1,
        
      },
      {
        "id": 2502,
        "question_type": "CheckboxQuestion",
        "prompt": "What is best way to reach?",
        "is_required": false,
        "fields": [{
          "text": "Skype",
          "value": 0,
          "id": "skype",
          "name": "commPref",
          "checked": true
        },{
          "text": "Email",
          "value": 1,
          "id": "email",
          "name": "commPref",
          "checked": false
        },
        {
          "text": "Phone",
          "value": 2,
          "id": "phone",
          "name": "commPref",
          "checked": true
        }]
        
      },
      {
        "id": 2501,
        "question_type": "RadioQuestion",
        "prompt": "What is your choice here?",
        "is_required": false,
        "fields": [{
          "text": "Kuala Lumpur",
          "value": 0,
          "id": "kl",
          "name": "city",
          "checked": true
        },{
          "text": "Penang Island",
          "value": 1,
          "id": "pi",
          "name": "city",
          "checked": false
        }]
      }
    ]
  }