## Features

- Supports Multiple Input Elements (Checkbox, Radio, Textarea etc.)
- Fully Extensible
- Navigation via Keyboard
- Progress Bar
- A Good UI and UX

Demo - [My Formy](http://myformy.herokuapp.com)

## My Approach 
Before starting code, I have planned about this on paper and then implemented. 

Formy will have 3 main components - 

1. Header: A simple component that takes a text and render on top of the form

2. Body: This is the main component which is responsible of  rendering different sub-components (Example: textarea, radios, checkboxes etc.). It also manage state of next button.

3. Footer: This is another simple component which is responsible for rendering next, prev and submit buttons. These buttons have `onClick` method attached to each buttons.

### Extensibility 
As each form elements are treated as sub-components, you can add as many form elements as required. Each component has their own way of validation and extra UI elements (Example: `TextArea.js` has extra chars to go text).

You just need to call `isNextDisabled` method.

### Modular
Each component and functions are sort of pure in the nature. The chance of side effect is very less and can be improved further.

### Readability 
Special attention is given to naming convention and commnents in the code.

### Latest and Greatest
Latest ES6 is used in the code. Even the CSS use custom variables to make design easy to modify and extend.
